angular.module('starter.controllers', [])

.controller('DashCtrl', function ($scope) { })

.controller('LoginCtrl', function ($scope, $state, $ionicHistory) {
    $scope.login = function (number) {
        localStorage.setItem("number", "");
        if (number.length == 10) {
            $ionicHistory.nextViewOptions({
                disableBack: true,
                disableAnimate: true,
                historyRoot: true
            });
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            localStorage.setItem("number", number);
            $state.go('tab.orders');
        }
        else {
            // show error
        }
    };
    if (localStorage.getItem("number") !== null && localStorage.getItem("number") !== "") {
        $ionicHistory.nextViewOptions({
            disableBack: true,
            disableAnimate: true,
            historyRoot: true
        });
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $state.go('tab.orders');
    }

})

.controller('OrdersCtrl', function ($scope, $http, $state) {
    if (localStorage.getItem("number") !== null && localStorage.getItem("number") !== "") {
        $scope.number = localStorage.getItem("number");
    }
    else {
        $state.go('login');
    }
    $scope.getOrders = function () {
        //if ($scope.Orders != undefined) $scope.Orders.splice(0);
        $scope.message = 'Loading Orders ..';
        var Orders = [];
        //$http.get("http://localhost:5403/api/Ordertracking/GetOrders", {
        $http.get("http://developserver.cloudapp.net:83/api/Ordertracking/GetOrders", {
            params: { MobileNumber: $scope.number }
        })
        .then(function (response) {
            angular.forEach(response.data, function (item, i) {
                var Order = {
                    OrderId: item.OrderId,
                    DriverId: item.DriverId,
                    OrderName: item.OrderName,
                    Status: item.Status,
                    OrderDate: item.OrderDate,
                    OrderCompleteDate: item.OrderCompleteDate
                }
                Orders.push(Order);
            });
            if (Orders.length == 0) {
                $scope.message = 'No orders found.';
            }
            $scope.Orders = Orders;
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    $scope.getOrders();
})

.controller('OrderDetailCtrl', function ($scope, $stateParams, $http) {
    var OrderIdParam = $stateParams.OrderId;
    var markers = [];
    var mapMarkers = [];
    var statusLines = [];
    var marker;
    var Compline;
    var Pendline;
    var completed = true;
    var Lastdate = new Date(1, 1, 1);
    var refresh;
    var lastPoint;
    var firstTime = true;
    var uluru = { lat: 30.726947, lng: 76.781723 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 3,
        center: uluru,
        styles: [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "landscape.man_made", "elementType": "geometry", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.natural.landcover", "elementType": "geometry", "stylers": [{ "visibility": "on" }, { "hue": "#ff0000" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "hue": "#1cff00" }] }, { "featureType": "poi.business", "elementType": "all", "stylers": [{ "visibility": "on" }] }, { "featureType": "poi.government", "elementType": "all", "stylers": [{ "visibility": "on" }, { "hue": "#ff0000" }] }, { "featureType": "poi.park", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": "-56" }, { "lightness": "76" }, { "gamma": "0.67" }, { "hue": "#ff0000" }] }, { "featureType": "poi.school", "elementType": "all", "stylers": [{ "visibility": "on" }, { "hue": "#001cff" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "hue": "#ff0000" }] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "visibility": "on" }, { "saturation": "46" }, { "lightness": "52" }, { "gamma": "5.46" }, { "weight": "0.32" }, { "color": "#00a5ff" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#37b3e6" }, { "visibility": "on" }] }]
    });
    StartTracking();

    function drawMarkers() {
        for (i = 0; i < markers.length; i++) {
            var position = new google.maps.LatLng(markers[i][0], markers[i][1]);
            marker = new google.maps.Marker({
                position: position,
                map: map
            });
            mapMarkers.push(marker);
        }
    }

    function drawLines() {
        Pendline = new google.maps.Polyline({
            path: [{ lat: markers[1][0], lng: markers[1][1] }, { lat: markers[2][0], lng: markers[2][1] }],
            strokeOpacity: 0,
            icons: [{
                icon: {
                    path: 'M 0,-1 0,1',
                    strokeOpacity: 1,
                    scale: 3,
                    strokeColor: '#FF0000',
                },
                offset: '0',
                repeat: '15px'
            }],
            map: map
        });

        statusLines.push(Pendline);
    }

    function removeObjs() {
        for (i = 0; i < mapMarkers.length; i++) {
            //if (mapMarkers[i].getPosition().lat() != markers[i][0]) {
            mapMarkers[i].setMap(null);
            //}
        }
        for (i = 0; i < statusLines.length; i++) {
            statusLines[i].setMap(null);
        }
        mapMarkers = [];
        statusLines = [];
    }

    function reloadMap(temp) {
        markers = temp;
        removeObjs();
        drawMarkers();
        drawLines();

        if (firstTime) {
            firstTime = false;
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < mapMarkers.length; i++) {
                bounds.extend(mapMarkers[i].getPosition());
            }
            map.fitBounds(bounds);
        }
    }

    function drawRoute(routeList) {
        if (routeList != null) {
            for (var i = 0; i < routeList.length - 1; i++) {
                drawLine(routeList[i]['Latitude'], routeList[i]['Longitude'], routeList[i + 1]['Latitude'], routeList[i + 1]['Longitude']);
            }
        }
        if (lastPoint != null) {
            drawLine(routeList[0]['Latitude'], routeList[0]['Longitude'], lastPoint[0], lastPoint[1]);
        }
        lastPoint = [routeList[routeList.length - 1]['Latitude'], routeList[routeList.length - 1]['Longitude']];
    }

    function drawLine(a, b, c, d) {
        Compline = new google.maps.Polyline({
            path: [{ lat: a, lng: b }, { lat: c, lng: d }],
            geodesic: true,
            strokeColor: '#393',
            strokeOpacity: 1.0,
            strokeWeight: 3,
            map: map
        });
    }

    function getPoints() {
        if (completed) {
            completed = false;
            var temp;
            $http.get("http://localhost:5403/api/Ordertracking/DriverLocation", {
            //$http.get("http://developserver.cloudapp.net:83/api/Ordertracking/DriverLocation", {
                params: { OrderId: OrderIdParam, LastDate: Lastdate }
            })
            .then(function (response) {
                if (response.data.Status == 'Delivered') {
                    if (refresh)
                        clearInterval(refresh);
                }
                completed = true;
                var temp = [
                    [response.data.StartLatitude, response.data.StartLongitude],
                    [response.data.CurrentLatitude, response.data.CurrentLongitude],
                    [response.data.EndLatitude, response.data.EndLongitude],
                ];
                Lastdate = response.data.LastDate;
                reloadMap(temp);
                drawRoute(response.data.ListDriverLocation);
            });
        }
    }

    function StartTracking() {
        getPoints();
        refresh = setInterval(getPoints, 30000);
    };

    $scope.$on('$destroy',function(){
        if(refresh)
            clearInterval(refresh);
    });

})

.controller('AccountCtrl', function ($scope, $state, $http) {

});